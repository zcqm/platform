#platform
##使用到的相关技术：
1.    SiteMesh 2                 
2.    Bootstrap Multiselect      http://davidstutz.github.io/bootstrap-multiselect/
3.    bootstrap3-dialog          http://nakupanda.github.io/bootstrap3-dialog/
4.    Date Picker                http://bootstrap-datepicker.readthedocs.org/en/stable/
	datetime           	 http://eonasdan.github.io/bootstrap-datetimepicker/
5.    bootstrapValidator         http://bv.doc.javake.cn/api/
6.    noUiSlider                 http://refreshless.com/nouislider/slider-options/
7.    fuelux                     http://getfuelux.com/javascript.html#wizard-usage-markup
8.    jQuery Jcrop v0.9.12           //图片裁剪器
9.    zTree                      http://www.treejs.cn/v3/main.php
10    Font Awesome               http://fontawesome.dashgame.com/
